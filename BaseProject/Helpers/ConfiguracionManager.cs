﻿using BaseProject.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Helpers
{
    public class ConfiguracionManager
    {
        public static string GetConfiguracion(string llave)
        {
            using (var context = new ProyectoBaseEntities())
            {
                return context.Configuracion.FirstOrDefault(x => x.Descripcion == llave)?.Valor;
            }
        }

        public static int GetConfiguracionId(string llave)
        {
            using (var context = new ProyectoBaseEntities())
            {
                return Convert.ToInt32(context.Configuracion.FirstOrDefault(x => x.Descripcion == llave)?.Valor ?? "");
            }
        }


        public static decimal GetConfiguracionDecimal(string llave)
        {
            try
            {
                using (var context = new ProyectoBaseEntities())
                {
                    return Convert.ToDecimal(context.Configuracion.FirstOrDefault(x => x.Descripcion == llave)?.Valor ?? "");
                }
            }
            catch (Exception e)
            {
                return 0m;
            }

        }
    }
}