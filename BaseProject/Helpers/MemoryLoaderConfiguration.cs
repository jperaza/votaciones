﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Helpers
{
    public static class MemoryLoaderConfiguration
    {
        public static int EstadoTarea_Pendiente = 1;
        public static int EstadoTarea_EnProceso = 2;
        public static int EstadoTarea_Completado = 3;
    }
}