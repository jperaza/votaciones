﻿using BaseProject.DB;
using BaseProject.Models;
using BaseProject.Models.Votacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Controllers
{
    public class VotacionController : Controller
    {
        // GET: Votacion
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ValidarEstudiante(VotacionViewModel model)
        {
            using(var db = new VotacionesEntities())
            {
                var existe = db.Alumnos.FirstOrDefault(x => x.Identidad == model.Identidad);

                if (existe == null)
                    return Json(new ResponseMessage { Ok = false, Message = $"Error no se encontro ningun alumno con el numero de identitdad {model.Identidad}" });

                var Yavoto = db.Votos.FirstOrDefault(x => x.AlumnoId == existe.Id);

                if (Yavoto != null)
                    return Json(new ResponseMessage { Ok = false, Message = $"El estudiante {existe.Nombre} ya voto!" });


                var hayPlanilla = db.Planilla.FirstOrDefault(x => x.ModalidadId == existe.ModalidadId);

                if(hayPlanilla == null)
                    return Json(new ResponseMessage { Ok = false, Message = $"Error no se encontro ninguna planilla para la modalidad del alumno" });

                return Json(new ResponseMessage { Ok = true, Message = $"" });
            }
        }

        [HttpGet]
        public ActionResult VotacionPlanilla(string identidad)
        {
            using(var db = new VotacionesEntities())
            {
                var existe = db.Alumnos.FirstOrDefault(x => x.Identidad == identidad);

                if (existe == null)
                    return RedirectToAction("index");

                var Yavoto = db.Votos.FirstOrDefault(x => x.AlumnoId == existe.Id);

                if(Yavoto != null)
                    return RedirectToAction("index");


                var estudiante = db.Alumnos.Where(x => x.Identidad == identidad).Select(x => new VotacionPlanilla
                {
                    Id = x.Id,
                    Grado = x.Grado,
                    Identidad = x.Identidad,
                    Jornada = x.Jornada,
                    ModalidadId = x.ModalidadId,
                    Nombre = x.Nombre,
                    Seccion = x.Seccion
                }).FirstOrDefault();

                var listaPlanilla = db.Planilla.Where(x => x.ModalidadId == estudiante.ModalidadId & x.AdjuntoStream != null).Select(x => new PlanillaEnc
                {
                    Id = x.Id,
                    Logo = x.Eslogan,
                    PlanillaNombre = x.Planilla1

                }).ToList();

                listaPlanilla.ForEach(x => x.lstDetalle = db.Planilla_Detalle.Where(f => f.PlanillaId == x.Id).Select(f => new PlanillaDet
                        {
                            Alumno = f.Alumnos.Nombre,
                            CargoId = f.CargoId,
                            Cargo = f.Cargos.Descripcion
                        }).ToList()
                    );

                estudiante.lstPlanilla = listaPlanilla;
                
                return View(estudiante);

            }
            
        }

        public ActionResult ObtenerEsloganPlanilla(int idPlanilla)
        {
            using(var db = new VotacionesEntities())
            {
                var model = db.Planilla.FirstOrDefault(x => x.Id == idPlanilla);

                return File(model.AdjuntoStream, "image/jpeg");
            }
        }

        public ActionResult ObtenerImagenesCandidatos(int idPlanilla, int idCandidato)
        {
            using(var db = new VotacionesEntities())
            {
                var detalle = db.Planilla_Detalle.FirstOrDefault(x => x.PlanillaId == idPlanilla & x.CargoId == idCandidato);

                return File(detalle.AdjuntoStram, "image/jpeg");
            }
        }

        [HttpPost]
        public ActionResult GuardarVotacion(int IdAlumno, int IdPlanilla)
        {
            using (var db = new VotacionesEntities())
            {
                var voto = new Votos
                {
                    AlumnoId = IdAlumno,
                    PlanillaId = IdPlanilla
                };

                db.Votos.Add(voto);

                db.SaveChanges();

                return Json(new ResponseMessage { Ok = true, Message = "Su voto se ha guardado exitosamente!" });

            }
        }


    }
}