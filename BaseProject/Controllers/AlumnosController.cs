﻿using BaseProject.DB;
using BaseProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Controllers
{
    [Authorize(Roles = "it_mantenimiento")]
    public class AlumnosController : Controller
    {
        // GET: Alumnos
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ObtenerTabla()
        {
            using(var db = new VotacionesEntities())
            {
                var data = db.Alumnos.Select(x => new AlumnosViewModel
                {
                    Id = x.Id,
                    Grado = x.Grado,
                    Identidad = x.Identidad,
                    Jornada = x.Jornada,
                    ModalidadId = x.ModalidadId,
                    Nombre = x.Nombre,
                    Seccion = x.Seccion
                }).ToList();

                return Json(new { data = data }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpGet]
        public ActionResult CrearAlumno()
        {
            return PartialView(new AlumnosViewModel());
        }

        [HttpPost]
        public ActionResult CrearAlumno(AlumnosViewModel model)
        {
            try
            {
                using (var db = new VotacionesEntities())
                {

                    var existe = db.Alumnos.FirstOrDefault(x => x.Identidad == model.Identidad);

                    if (existe != null)
                        return Json(new ResponseMessage { Ok = false, Message = $"El numero de identidad {model.Identidad} ya se encuentra registrado" });

                    var alumno = new Alumnos
                    {
                        Grado = model.Grado,
                        Identidad = model.Identidad,
                        Jornada = model.Jornada,
                        ModalidadId = model.ModalidadId,
                        Nombre = model.Nombre,
                        Seccion = model.Seccion
                    };

                    db.Alumnos.Add(alumno);
                    db.SaveChanges();

                    return Json(new ResponseMessage { Ok = true, Message = $"Alumno {model.Nombre} creado exitosamente!" });

                }
            }
            catch(Exception ex) { return Json(new ResponseMessage { Ok = false, Message = $"Error {ex.Message}" }); }
        }

        [HttpGet]
        public ActionResult EditarAlumno(int Id)
        {
            using(var db = new VotacionesEntities())
            {
                var alumno = db.Alumnos.Where(x => x.Id == Id).Select(x => new AlumnosViewModel
                {
                    Id = x.Id,
                    Grado = x.Grado,
                    Identidad = x.Identidad,
                    Jornada = x.Jornada,
                    ModalidadId = x.ModalidadId,
                    Nombre = x.Nombre,
                    Seccion = x.Seccion
                }).FirstOrDefault();

                return PartialView(alumno);
            }
        }

        [HttpPost]
        public ActionResult EditarAlumno(AlumnosViewModel model)
        {
            using(var db = new VotacionesEntities())
            {
                var alumno = db.Alumnos.FirstOrDefault(x => x.Id == model.Id);

                alumno.Identidad = model.Identidad;
                alumno.Nombre = model.Nombre;
                alumno.Seccion = model.Seccion;
                alumno.Jornada = model.Jornada;
                alumno.ModalidadId = model.ModalidadId;
                alumno.Grado = model.Grado;

                db.SaveChanges();

                return Json(new ResponseMessage { Ok = true, Message = "Alumno editado exitosamente!" });

            }
        }



    }
}