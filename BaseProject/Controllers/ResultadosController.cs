﻿using BaseProject.DB;
using BaseProject.Models.Resultados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Controllers
{
    [Authorize(Roles = "resultados_primaria,resultados_secundaria")]
    public class ResultadosController : Controller
    {
        // GET: Resultados
        public ActionResult Index(int idModalidad)
        {
            using(var db = new VotacionesEntities())
            {
                var Planillas = db.Planilla.Where(x => x.ModalidadId == idModalidad & x.AdjuntoStream != null).ToList();
                var lstPlanillas = new List<ResultadosPlanilla>();

                Planillas.ForEach(x =>
                {
                    lstPlanillas.Add(new ResultadosPlanilla
                    {
                        Planilla = x.Planilla1,
                        Total = db.Votos.Where(f => f.PlanillaId == x.Id).Count()
                    });
                });

                var resultado = new PlanillaEncViewModel { idModalidad = idModalidad };

                resultado.lstPlanilla = lstPlanillas;


                return View(resultado);
            }
            
        }

        public ActionResult GetTabla(int idModalidad)
        {
            using(var db = new VotacionesEntities())
            {
                var data = db.Votos.Where(x => x.Alumnos.ModalidadId == idModalidad).Select(x => new InformeResultadosViewModel { 
                    Estudiante = x.Alumnos.Nombre,
                    Planilla = x.Planilla.Planilla1,
                    Grado = x.Alumnos.Grado,
                    Jornada = x.Alumnos.Jornada,
                    Seccion = x.Alumnos.Seccion
                }).ToList();

                return Json(new { data = data }, JsonRequestBehavior.AllowGet);
            }
        }

        


    }
}