﻿using BaseProject.DB;
using BaseProject.Models.Resultados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Controllers
{
    
    [Authorize(Roles = "estadisticas_primaria,estadisticas_secundaria")]
    public class EstadisticasController : Controller
    {
        // GET: Estadisticas
        public ActionResult Index(int idModalidad)
        {
            using (var db = new VotacionesEntities())
            {
                var Planillas = db.Planilla.Where(x => x.ModalidadId == idModalidad & x.AdjuntoStream != null).ToList();
                var lstPlanillas = new List<ResultadosPlanilla>();

                Planillas.ForEach(x =>
                {
                    lstPlanillas.Add(new ResultadosPlanilla
                    {
                        Planilla = x.Planilla1,
                        Total = db.Votos.Where(f => f.PlanillaId == x.Id).Count()
                    });
                });

                var resultado = new PlanillaEncViewModel { idModalidad = idModalidad };

                resultado.lstPlanilla = lstPlanillas;


                return View(resultado);
            }
        }

        public ActionResult ObtenerGrafico(int idModalidad)
        {
            using (var db = new VotacionesEntities())
            {
                var Planillas = db.Planilla.Where(x => x.ModalidadId == idModalidad & x.AdjuntoStream != null).ToList();
                var lstPlanillas = new List<ResultadosPlanilla>();

                Planillas.ForEach(x =>
                {
                    lstPlanillas.Add(new ResultadosPlanilla
                    {
                        Planilla = x.Planilla1,
                        Total = db.Votos.Where(f => f.PlanillaId == x.Id).Count()
                    });
                });

                var resultado = new PlanillaEncViewModel
                {
                    idModalidad = idModalidad,
                    CantidadVotosTotales = db.Votos.Where(x => x.Alumnos.ModalidadId == idModalidad).Count()
                };

                resultado.lstPlanilla = lstPlanillas;

                return Json(resultado, JsonRequestBehavior.AllowGet);

            }
        }


    }
}