﻿using BaseProject.DB;
using BaseProject.Models;
using BaseProject.Models.Planilla;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Controllers
{
    [Authorize(Roles = "it_mantenimiento")]
    public class PlanillaController : Controller
    {
        // GET: Planilla
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ObtenerTabla()
        {
            using(var db = new VotacionesEntities())
            {
                var data = db.Planilla.Select(x => new PlanillaViewModel
                {
                    Eslogan = x.Eslogan,
                    Id = x.Id,
                    ModalidadId = x.ModalidadId,
                    Planilla = x.Planilla1
                }).ToList();

                return Json(new { data = data }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult CrearPlanilla()
        {
            using(var db = new VotacionesEntities())
            {
                var lstAlumnos = db.Alumnos.Where(x => x.ModalidadId == 1).AsEnumerable().Select(x => new SelectListItem
                {
                    Text = $"{x.Identidad} - {x.Nombre}",
                    Value = x.Id.ToString()
                }).ToList();

                return View(new PlanillaViewModel { 
                    lstAlumnos = lstAlumnos
                });
            }
        }

        [HttpPost]
        public ActionResult CrearPlanilla(PlanillaViewModel model)
        {
            using(var db = new VotacionesEntities())
            {
                var planilla = new Planilla
                {
                    Eslogan = model.Eslogan,
                    ModalidadId = model.ModalidadId,
                    Planilla1 = model.Planilla
                };

                db.Planilla.Add(planilla);
                db.SaveChanges();

                model.lstDetalle.ForEach(x =>
                {
                    var detalle = new Planilla_Detalle
                    {
                        PlanillaId = planilla.Id,
                        AlumnoId = x.AlumnoId,
                        CargoId = x.CargoId,
                        AdjuntoStram = System.Convert.FromBase64String(x.base64)
                    };

                    db.Planilla_Detalle.Add(detalle);
                    db.SaveChanges();
                });


                return Json(new { id = planilla.Id }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpGet]
        public ActionResult AlumnosPorModalidad(int idModalidad)
        {
            using(var db = new VotacionesEntities())
            {
                var lstAlumnos = db.Alumnos.Where(x => x.ModalidadId == idModalidad).AsEnumerable().Select(x => new SelectListItem
                {
                    Text = $"{x.Identidad} - {x.Nombre}",
                    Value = x.Id.ToString()
                }).ToList();

                return Json(lstAlumnos, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SubirAdjuntoPlanilla(int idPlanilla)
        {
            using (var db = new VotacionesEntities())
            {

                foreach (string fileName in Request.Files)
                {
                    var file = Request.Files[fileName];
                    if (file == null) continue;
                    var tipoContenido = file.ContentType;
                    var adjunto = new BinaryReader(file.InputStream).ReadBytes(Convert.ToInt32(file.InputStream.Length));

                    var planilla = db.Planilla.FirstOrDefault(x => x.Id == idPlanilla);

                    planilla.AdjuntoStream = adjunto;

                    db.SaveChanges();
                }
                   
                return null;
            }
        }

        public ActionResult EditarPlanilla(int idPlanilla)
        {
            using(var db = new VotacionesEntities())
            {

                var planillaEnc = db.Planilla.Where(x => x.Id == idPlanilla).Select(x => new PlanillaViewModel { 
                    Id = x.Id,
                    Eslogan = x.Eslogan,
                    ModalidadId = x.ModalidadId,
                    Planilla = x.Planilla1
                }).FirstOrDefault();


                var lstAlumnos = db.Alumnos.Where(x => x.ModalidadId == planillaEnc.ModalidadId).AsEnumerable().Select(x => new SelectListItem
                {
                    Text = $"{x.Identidad} - {x.Nombre}",
                    Value = x.Id.ToString()
                }).ToList();

                planillaEnc.lstAlumnos = lstAlumnos;

                return View(planillaEnc);

            }
        }

        [HttpPost]
        public ActionResult EditarPlanilla(PlanillaViewModel model)
        {
            try
            {
                using (var db = new VotacionesEntities())
                {
                    var planilla = db.Planilla.FirstOrDefault(x => x.Id == model.Id);
                    planilla.Planilla1 = model.Planilla;
                    planilla.ModalidadId = model.ModalidadId;
                    planilla.Eslogan = model.Eslogan;

                    db.SaveChanges();

                    return Json(new ResponseMessage { Ok = true, Message = $"{model.Id}" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception ex) { return Json(new ResponseMessage { Ok = false, Message = $"error" }, JsonRequestBehavior.AllowGet); }
        }

        public ActionResult ObtenerAdjunto(int idPlanilla)
        {
            using(var db = new VotacionesEntities())
            {
                var adjunto = db.Planilla.Where(x => x.Id == idPlanilla).AsEnumerable().Select(x => new PlanillaAdjuntoViewModel
                {
                    Id = x.Id,
                    AdjuntoStream = x.AdjuntoStream,
                    Tipo = "image/jpeg",
                    Nombre = $"{x.Planilla1}{x.Id}",
                    AdjuntoLength = Convert.ToBase64String(x.AdjuntoStream).Length.ToString()
                }).FirstOrDefault();

                return Json(adjunto, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DescargarAdjunto(int idPlanilla)
        {
            using(var db = new VotacionesEntities())
            {
                var adjunto = db.Planilla.FirstOrDefault(x => x.Id == idPlanilla);

                return File(adjunto.AdjuntoStream, "image/jpeg", $"{adjunto.Planilla1}{adjunto.Id}");
            }
        }

        public JsonResult GetDetallePlanilla(int idPlanilla)
        {
            using(var db = new VotacionesEntities())
            {
                var detalles = db.Planilla_Detalle.Where(x => x.PlanillaId == idPlanilla).Select(x => new DetallePlanilla
                {
                    Id = x.Id,
                    Alumno = x.Alumnos.Nombre,
                    Cargo = x.Cargos.Descripcion,
                    AlumnoId = x.AlumnoId
                }).ToList();

                return Json(new { data = detalles }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ObtenerImagen(int Id)
        {
            using (var db = new VotacionesEntities())
            {
                var detalles = db.Planilla_Detalle.FirstOrDefault(x => x.Id == Id);

                return File(detalles.AdjuntoStram, "image/jpeg");
            }
        }

        [HttpPost]
        public JsonResult AgregarCargoEstudiante(DetallePlanilla detalle)
        {
            try
            {
                using(var db = new VotacionesEntities())
                {
                    var planilla = new Planilla_Detalle
                    {
                        AlumnoId = detalle.AlumnoId,
                        CargoId = detalle.CargoId,
                        AdjuntoStram = System.Convert.FromBase64String(detalle.base64),
                        PlanillaId = detalle.idPlanilla
                    };

                    db.Planilla_Detalle.Add(planilla);
                    db.SaveChanges();

                    return Json(new ResponseMessage { Ok = true, Message = "guardado" }, JsonRequestBehavior.AllowGet);

                }
            }catch(Exception ex) { return Json(new ResponseMessage { Ok = false, Message = "Error" }, JsonRequestBehavior.AllowGet); }
        }


        [HttpPost]
        public JsonResult EliminarCargoEstudiante(int idEstudiante, int idPlanilla)
        {
            try
            {
                using (var db = new VotacionesEntities())
                {
                    var data = db.Planilla_Detalle.FirstOrDefault(x => x.AlumnoId == idEstudiante & x.PlanillaId == idPlanilla);
                    db.Planilla_Detalle.Remove(data);
                    db.SaveChanges();
                    return Json(new ResponseMessage { Ok = true, Message = $"Borrado" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex) { return Json(new ResponseMessage { Ok = true, Message = $"error" }, JsonRequestBehavior.AllowGet); }
        }

    }
}