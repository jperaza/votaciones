﻿using BaseProject.Models;
using BaseProject.Models.Seguridad;
using BaseProject.Repositories.SeguridadManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Controllers
{
    [Authorize(Roles = "seguridad")]
    public class SeguridadController : Controller
    {
        private readonly ISeguridadManager _seguridad;

        public SeguridadController(ISeguridadManager seguridad)
        {
            _seguridad = seguridad;
        }

        #region Usuario

        // GET: Seguridad
        public ActionResult Usuario()
        {
            return View();
        }

        public JsonResult GetTablaUsuario()
        {
            return Json(new { data = _seguridad.ObtenerUsuarios() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CrearUsuario()
        {
            return PartialView(new UsuarioViewModel());
        }

        [HttpPost]
        public ActionResult CrearUsuario(UsuarioViewModel model)
        {
            var response = _seguridad.CrearUsuario(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult EditarUsuario(string usuario)
        {
            var data = _seguridad.ObtenerUsuario(usuario);
            return PartialView(data);
        }

        [HttpPost]
        public ActionResult EditarUsuario(UsuarioViewModel model)
        {
            var response = _seguridad.EditarUsuario(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ChangePassword(string usuario)
        {
            var model = _seguridad.ObtenerUsuario(usuario);
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult ChangePassword(UsuarioViewModel model)
        {
            var response = _seguridad.CambiarContrasena(model);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Roles

        public ActionResult Roles()
        {
            return View();
        }

        public JsonResult GetTablaRoles()
        {
            var data = _seguridad.ObtenerListaRoles();
            return Json(new { data = data}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CrearRol()
        {
            var model = _seguridad.ModelCrearRol();

            return PartialView(model);
        }

        [HttpPost]
        public ActionResult CrearRol(RolViewModel model)
        {
            var response = _seguridad.CrearRol(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult EditarRol(string RoleName)
        {
            var model = _seguridad.ObtenerRolPorNombre(RoleName);
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult EditarRol(RolViewModel model)
        {
            var response = _seguridad.EditarRol(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Sucursales

        public ActionResult Sucursales()
        {
            return View();
        }

        public JsonResult GetTablaSucursales()
        {
            return Json(new { data = _seguridad.ObtenerSucursales() }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CrearSucursal()
        {
            return PartialView(new SucursalViewModel { });
        }

        [HttpPost]
        public ActionResult CrearSucursal(SucursalViewModel model)
        {
            var response = _seguridad.CrearSucursal(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult EditarSucursal(int idSucursal)
        {
            var sucursal = _seguridad.ObtenerSucursalPorId(idSucursal);
            return PartialView(sucursal);
        }

        [HttpPost]
        public ActionResult EditarSucursal(SucursalViewModel model)
        {
            var response = _seguridad.EditarSucursal(model);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region PermisosPorUsuario

        public ActionResult AsignarPermisos(string usuario)
        {
            var datosUsuario = _seguridad.ObtenerUsuario(usuario);

            var model = new PermisosPorUsuarioViewModel
            {
                Nombre = datosUsuario.nombre,
                UserId = datosUsuario.UserId,
                Usuario = usuario
            };

            return PartialView(model);
        }

        public ActionResult ObtenerPermisos(Guid userId)
        {
            var data = _seguridad.ObtenerRoles(userId);

            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GuardarSeleccionPermisos(Guid UserId, List<PermisosPorUsuarioViewModel> ListaDetalle)
        {
            var response = _seguridad.AsignarRoles(UserId, ListaDetalle);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}