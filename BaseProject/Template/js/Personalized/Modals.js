﻿var IdSelectActual;
function VerModalCrearParaSelect(ActionName, IdSelect) {

    $.ajax({
        url: '/Mantenimientos/' + ActionName,
        type: 'GET',
        cache: false,
        success: function (data) {
            IdSelectActual = IdSelect;
            ShowModal("Crear Nuevo", data);
        }
    });
}

function ShowModalBig(Title, data) {
    $("#MyModalTitleBig").html(Title);
    $("#MyModalBodyBig").html(data);
    $("#MyModalBig").modal("show");

}

function ShowModal(data) {
    $("#MyModalContent").html(data);
    $("#MyModal").modal("show");

}

function ExpandirModal(boton) {
    $("#MyModalSize").toggleClass("modal-dialog");
}

function CerrarModal() {
    $("#MyModal").modal("hide");
    $("#MyModalBig").modal("hide");
    $("#MyModalBody").empty();
    $("#MyModalBodyBig").empty();
}


function VerModalVistaPrevia(id, url) {
    $.ajax({
        url: url,
        method: "get",
        data: {id:id},
        success: function (resp) {
            ShowModal(resp);
        },
        error: function (resp) {
            alert("Error");
        }
    });
}