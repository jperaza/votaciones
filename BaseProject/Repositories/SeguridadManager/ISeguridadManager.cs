﻿using BaseProject.Models;
using BaseProject.Models.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BaseProject.Repositories.SeguridadManager
{
    public interface ISeguridadManager
    {
        #region Sucursales
        List<SucursalViewModel> ObtenerSucursales();
        ResponseMessage CrearSucursal(SucursalViewModel sucursal);
        SucursalViewModel ObtenerSucursalPorId(int idSucursal);
        ResponseMessage EditarSucursal(SucursalViewModel model);
        #endregion

        #region Usuario

        List<UsuarioViewModel> ObtenerUsuarios();
        List<SelectListItem> Sucurales();
        ResponseMessage CrearUsuario(UsuarioViewModel model);
        UsuarioViewModel ObtenerUsuario(string usuario);
        ResponseMessage EditarUsuario(UsuarioViewModel model);
        ResponseMessage CambiarContrasena(UsuarioViewModel model);
        #endregion

        #region Rol

        Guid ObtenerApplicationId();
        List<RolViewModel> ObtenerListaRoles();
        RolViewModel ModelCrearRol();
        ResponseMessage CrearRol(RolViewModel model);
        RolViewModel ObtenerRolPorNombre(string RoleName);
        ResponseMessage EditarRol(RolViewModel model);
        #endregion

        #region Asignar Roles

        List<PermisosPorUsuarioViewModel> ObtenerRoles(Guid userId);
        ResponseMessage AsignarRoles(Guid UserId, List<PermisosPorUsuarioViewModel> ListaDetalle);

        #endregion
    }
}
