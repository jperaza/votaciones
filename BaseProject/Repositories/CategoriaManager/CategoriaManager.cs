﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BaseProject.DB;
using BaseProject.Models;
using BaseProject.Models.Categoria;

namespace BaseProject.Repositories.CategoriaManager
{
    public class CategoriaManager : ICategoriaManager
    {
        public ResponseMessage Crear(CategoriaViewModel model)
        {
            try
            {
                using (var db = new VidrieriaEntities())
                {
                    var categoria = new Categoria
                    {
                        Activo = model.Activo,
                        Descripcion = model.Descripcion
                    };

                    db.Categoria.Add(categoria);
                    db.SaveChanges();

                    return new ResponseMessage { Ok = true, Message = $"Categoria creada exitosamente!" };
                }
            }
            catch(Exception ex) { return new ResponseMessage { Ok = false, Message = $"Error {ex.Message.ToString()}" }; }
        }

        public ResponseMessage Editar(CategoriaViewModel model)
        {
            try
            {
                using (var db = new VidrieriaEntities())
                {
                    var categoria = db.Categoria.FirstOrDefault(x => x.Id == model.Id);

                    categoria.Descripcion = model.Descripcion;

                    db.SaveChanges();

                    return new ResponseMessage { Ok = true, Message = $"Categoria editada exitosamente!" };

                }
            }
            catch(Exception ex) { return new ResponseMessage { Ok = false, Message = $"Error {ex.Message.ToString()}" }; }
        }

        public List<SelectListItem> lstTipoCategoria()
        {
            using(var db = new VidrieriaEntities())
            {
                var model = db.Categoria.Select(x => new SelectListItem
                {
                    Text = x.Descripcion,
                    Value = x.Id.ToString()
                }).ToList();

                return model;
            }
        }

        public CategoriaViewModel ObtenerById(int idTipoMedida)
        {
            using(var db = new VidrieriaEntities())
            {
                var model = db.Categoria.Where(x => x.Id == idTipoMedida).Select(x => new CategoriaViewModel
                {
                    Activo = x.Activo,
                    Descripcion = x.Descripcion,
                    Id = x.Id
                }).FirstOrDefault();

                return model;
            }
        }

        public List<CategoriaViewModel> ObtenerListado()
        {
            using (var db = new VidrieriaEntities())
            {
                var model = db.Categoria.Select(x => new CategoriaViewModel
                {
                    Activo = x.Activo,
                    Descripcion = x.Descripcion,
                    Id = x.Id
                }).ToList();

                return model;
            }
        }
    }
}