﻿using BaseProject.Models;
using BaseProject.Models.Categoria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BaseProject.Repositories.CategoriaManager
{
    public interface ICategoriaManager
    {
        List<CategoriaViewModel> ObtenerListado();
        ResponseMessage Crear(CategoriaViewModel model);
        CategoriaViewModel ObtenerById(int idTipoMedida);
        ResponseMessage Editar(CategoriaViewModel model);
        List<SelectListItem> lstTipoCategoria();
    }
}
