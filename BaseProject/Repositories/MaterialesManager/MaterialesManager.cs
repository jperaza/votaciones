﻿using BaseProject.DB;
using BaseProject.Models;
using BaseProject.Models.Materiales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Repositories.MaterialesManager
{
    public class MaterialesManager : IMaterialesManager
    {

        public List<MaterialesViewModel> GetTablaMateriales()
        {
            using(var db = new VidrieriaEntities())
            {
                var model = db.Materiales.Select(x => new MaterialesViewModel
                {
                    Activo = x.Activo,
                    Nombre = x.Nombre,
                    DescripcionActivo = (x.Activo) ? "Activo" : "Inactivo",
                    Codigo = x.Codigo,
                    Descripcion = x.Descripcion,
                    Id = x.Id,
                    TipoMedidaId = x.TipoMedidaId,
                    TipoMedida = x.TipoMedida.Nombre,
                    Marca = x.Marca
                }).ToList();

                return model;
            }
        }

        public ResponseMessage CrearMetarial(MaterialesViewModel model)
        {
            try
            {
                using (var db = new VidrieriaEntities())
                {
                    var material = new Materiales
                    {
                        Activo = true,
                        Codigo = model.Codigo,
                        Nombre = model.Nombre,
                        Descripcion = model.Descripcion,
                        TipoMedidaId = model.TipoMedidaId,
                        Marca = model.Marca
                    };

                    db.Materiales.Add(material);
                    db.SaveChanges();

                    return new ResponseMessage { Ok = true, Message = $"El Material {model.Nombre} guardado exitosamente!" };

                }
            }
            catch(Exception ex) { return new ResponseMessage { Ok = false, Message = "Error al tratar de guardar el material" }; }
        }

        public MaterialesViewModel ObtenerMaterial(int idMaterial)
        {
            using(var db = new VidrieriaEntities())
            {
                var model = db.Materiales.Where(x => x.Id == idMaterial).Select(x => new MaterialesViewModel
                {
                    Activo = x.Activo,
                    Codigo = x.Codigo,
                    Descripcion = x.Descripcion,
                    Id = x.Id,
                    Nombre = x.Nombre,
                    TipoMedidaId = x.TipoMedidaId,
                    Marca = x.Marca
                }).FirstOrDefault();

                return model;
            }
        }

        public ResponseMessage EditarMaterial(MaterialesViewModel model)
        {
            try
            {
                using (var db = new VidrieriaEntities())
                {
                    var material = db.Materiales.FirstOrDefault(x => x.Id == model.Id);

                    material.Nombre = model.Nombre;
                    material.TipoMedidaId = model.TipoMedidaId;
                    material.Codigo = model.Codigo;
                    material.Descripcion = model.Descripcion;
                    material.Marca = model.Marca;
                    db.SaveChanges();

                    return new ResponseMessage { Ok = true, Message = "Material editado exitosamente" };
                }
            }
            catch(Exception ex) { return new ResponseMessage { Ok = true, Message = "Error al editar el material" }; }
        }

        public ResponseMessage CambiarEstado(int idMaterial)
        {
            using(var db = new VidrieriaEntities())
            {
                var material = db.Materiales.FirstOrDefault(x => x.Id == idMaterial);

                material.Activo = !material.Activo;
                var response = material.Activo ? "Habilitado" : "Inhabilitado";

                db.SaveChanges();

                return new ResponseMessage { Ok = true, Message = $"Material {response} exitosamente!" };
            }
        }

    }
}