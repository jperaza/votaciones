﻿using BaseProject.Models;
using BaseProject.Models.TipoMedida;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BaseProject.Repositories.TipoMedidaManager
{
    public interface ITipoMedidaManager
    {
        List<TipoMedidaViewModel> ObtenerListado();
        ResponseMessage CrearTipoMedida(TipoMedidaViewModel model);
        TipoMedidaViewModel ObtenerTipoMedida(int idTipoMedida);
        ResponseMessage EditarTipoMedida(TipoMedidaViewModel model);
        List<SelectListItem> lstTipoMedidas();
    }
}
