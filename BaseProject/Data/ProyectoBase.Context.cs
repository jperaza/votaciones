﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BaseProject.Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class ProyectoBaseEntities : DbContext
    {
        public ProyectoBaseEntities()
            : base("name=ProyectoBaseEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Articulo> Articulo { get; set; }
        public virtual DbSet<Seccion> Seccion { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<AvatarUsuario> AvatarUsuario { get; set; }
        public virtual DbSet<CategoriaRegistro> CategoriaRegistro { get; set; }
        public virtual DbSet<Cuentas> Cuentas { get; set; }
        public virtual DbSet<CuentasPorUsuario> CuentasPorUsuario { get; set; }
        public virtual DbSet<TipoRegistro> TipoRegistro { get; set; }
        public virtual DbSet<Configuracion> Configuracion { get; set; }
        public virtual DbSet<PrivilegiosPorRol> PrivilegiosPorRol { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<RolesPorUsuario> RolesPorUsuario { get; set; }
        public virtual DbSet<RedDeUsuarios> RedDeUsuarios { get; set; }
        public virtual DbSet<Usuarios> Usuarios { get; set; }
        public virtual DbSet<DetalleObjetivosUsuario> DetalleObjetivosUsuario { get; set; }
        public virtual DbSet<ObjetivosUsuario> ObjetivosUsuario { get; set; }
        public virtual DbSet<RegistroCuenta> RegistroCuenta { get; set; }
        public virtual DbSet<EstadoTareaProyecto> EstadoTareaProyecto { get; set; }
        public virtual DbSet<Proyecto> Proyecto { get; set; }
        public virtual DbSet<TipoProyecto> TipoProyecto { get; set; }
        public virtual DbSet<UsuariosPorProyecto> UsuariosPorProyecto { get; set; }
        public virtual DbSet<TareaProyecto> TareaProyecto { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<FaseProyecto> FaseProyecto { get; set; }
    }
}
