//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BaseProject.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class CuentasPorUsuario
    {
        public int Pk_IdCuentaPorUsuario { get; set; }
        public int Fk_IdUsuario { get; set; }
        public int Fk_IdCuenta { get; set; }
        public bool Activo { get; set; }
        public bool UsuarioPrincipal { get; set; }
    
        public virtual Cuentas Cuentas { get; set; }
        public virtual Usuarios Usuarios { get; set; }
    }
}
