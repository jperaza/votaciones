//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BaseProject.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class ObjetivosUsuario
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ObjetivosUsuario()
        {
            this.DetalleObjetivosUsuario = new HashSet<DetalleObjetivosUsuario>();
        }
    
        public int Pk_IdObjetivo { get; set; }
        public string Descripcion { get; set; }
        public string Icono { get; set; }
        public decimal ValorTotal { get; set; }
        public System.DateTime FechaCreado { get; set; }
        public Nullable<System.DateTime> FechaFinal { get; set; }
        public int Fk_IdUsuarioPrincipal { get; set; }
        public Nullable<int> IdUsuarioKid { get; set; }
        public bool ObjetivoBPKid { get; set; }
        public string Nota { get; set; }
        public bool Activo { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetalleObjetivosUsuario> DetalleObjetivosUsuario { get; set; }
        public virtual Usuarios Usuarios { get; set; }
        public virtual Usuarios Usuarios1 { get; set; }
    }
}
