﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Models.Articulo
{
    public class ListarArticuloViewModel : CrearArticuloViewModel
    {
        public string NombreSeccion { get; set; }

        public string ImageBase64 { get; set; }

    }
}