﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BaseProject.Models.Usuario
{
    public class CrearUsuarioViewModel
    {
        public int Id { get; set; }
        public string IdAspnetUser { get; set; }

        [Required]
        public string Nombre { get; set; }

        [Required]
        public string Apellido { get; set; }

        [Required]
        [Display(Name = "Fecha de Nacimiento")]
        public System.DateTime FechaNacimiento { get; set; }

        public string Telefono { get; set; }

        public System.DateTime FechaCreado { get; set; }

        public bool Activo { get; set; }

        public Nullable<int> Fk_IdAvatarUsuario { get; set; }

        //aspnetuser
        [Required]
        [Display(Name = "Correro Electronico")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Nombre de Usuario")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Rol de Usuario")]
        public int IdRol { get; set; }

        [Display(Name = "Rol")]
        public string NombreRol { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "El {0} tiene que ser al menos de {2} caracteres.", MinimumLength = 4)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Contraseña")]
        [Compare("Password", ErrorMessage = "la contraseña y la confirmacion no son el mismo")]
        public string ConfirmPassword { get; set; }


    }
}