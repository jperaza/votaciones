﻿using BaseProject.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Models.Planilla
{
    public class PlanillaViewModel
    {
        public int Id { get; set; }
        public string Planilla { get; set; }
        public string Eslogan { get; set; }
        public int ModalidadId { get; set; }
        public List<SelectListItem> lstAlumnos { get; set; }
        public List<SelectListItem> lstCargo { 
            get { return ObtenerListadoCargo(); }
            set { }
        }
        public List<DetallePlanilla> lstDetalle { get; set; }
        public string Modalidad
        {
            get { return ObtenerListadoModalidad(ModalidadId); }
            set { }
        }

        public List<SelectListItem> lst
        {
            get { return ObtenerListadoModalidad(); }
            set { }
        }


        public List<SelectListItem> ObtenerListadoModalidad()
        {
            List<SelectListItem> lst = new List<SelectListItem>();

            lst.Add(new SelectListItem
            {
                Text = "Primaria",
                Value = "1"
            });

            lst.Add(new SelectListItem
            {
                Text = "Secundaria",
                Value = "2"
            });

            return lst;

        }

        public string ObtenerListadoModalidad(int idModalidad)
        {
            return (idModalidad == 1) ? "Primaria" : "Secundaria";
        }

        public List<SelectListItem> ObtenerListadoCargo()
        {
            using(var db = new VotacionesEntities())
            {
                var lst = db.Cargos.Select(x => new SelectListItem
                {
                    Text = x.Descripcion,
                    Value = x.Id.ToString()
                }).ToList();

                return lst;
            }
        }

    }


    public class DetallePlanilla
    {
        public int Id { get; set; }
        public int AlumnoId { get; set; }
        public string Alumno { get; set; }
        public int CargoId { get; set; }
        public string Cargo { get; set; }
        public string base64 { get; set; }
        public int idPlanilla { get; set; }


    }

}