﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Models.Planilla
{
    public class PlanillaAdjuntoViewModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public byte[] AdjuntoStream { get; set; }
        public string AdjuntoLength { get; set; }

    }
}