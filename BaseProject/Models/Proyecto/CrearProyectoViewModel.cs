﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BaseProject.Models.Proyecto
{
    public class CrearProyectoViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Descripcion { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public bool Activo { get; set; }
        public bool Estado { get; set; }

        [Display(Name ="Tipo de Proyecto")]
        public int IdTipoProyecto { get; set; }

        public bool EsEditar { get; set; }
    }
}