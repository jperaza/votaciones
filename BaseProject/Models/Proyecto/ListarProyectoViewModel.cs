﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Models.Proyecto
{
    public class ListarProyectoViewModel : CrearProyectoViewModel
    {
        public string DescripcionTipoProyecto { get; set; }
        public int PorcentajeTerminado { get; set; }

        public int TotalTareasPendientes { get; set; }
        public int TotalTareasEnProceso { get; set; }
        public int TotalTareasCompletadas { get; set; }


        public List<ListarFaseProyectoViewModel> ListaFases = new List<ListarFaseProyectoViewModel>();
    }
}