﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Models.TareaProyecto
{
    public class ListarTareaProyectoViewModel : CrearTareaProyectoViewModel
    {
        public string DescripcionFaseProyecto { get; set; }
        public string DescripcionEstadoTareaProyecto { get; set; }
    }
}