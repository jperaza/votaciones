﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BaseProject.Models.TareaProyecto
{
    public class CrearTareaProyectoViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Descripcion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public bool Estado { get; set; }

        [Required]
        [Display(Name = "Fase de Proyecto")]
        public int IdFaseProyecto { get; set; }
        public int IdEstadoTareaProyecto { get; set; }

        public bool EsEditar { get; set; }
    }
}