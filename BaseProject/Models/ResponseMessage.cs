﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Models
{
    public class ResponseMessage
    {
        public bool Ok { get; set; }
        public string Message { get; set; }
    }
}