﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Models.Seguridad
{
    public class UsuarioViewModel
    {
        public Guid UserId { get; set; }
        public string usuario { get; set; }
        public string nombre { get; set; }
        public string email { get; set; }
        public string cargo { get; set; }
        public string contrasena { get; set; }
    }
}