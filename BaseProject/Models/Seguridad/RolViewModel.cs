﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Models.Seguridad
{
    public class RolViewModel
    {
        public Guid idAplicaccion { get; set; }
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public string descripcion { get; set; }
    }
}