﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseProject.Models
{
    public class AlumnosViewModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Identidad { get; set; }
        public string Jornada { get; set; }
        public string Grado { get; set; }
        public string Seccion { get; set; }
        public int ModalidadId { get; set; }
        public string Modalidad {
            get { return ObtenerListadoModalidad(ModalidadId); }
            set { }
        }

        public List<SelectListItem> lst {
            get { return ObtenerListadoModalidad(); } 
            set { }
        }


        public List<SelectListItem> ObtenerListadoModalidad()
        {
            List<SelectListItem> lst = new List<SelectListItem>();

            lst.Add(new SelectListItem
            {
                Text = "Primaria",
                Value = "1"
            });

            lst.Add(new SelectListItem
            {
                Text = "Secundaria",
                Value = "2"
            });

            return lst;

        }

        public string ObtenerListadoModalidad(int idModalidad)
        {
            return (idModalidad == 1) ? "Primaria" : "Secundaria";
        }

    }
}