﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Models.Roles
{
    public class ListaPermisosPorRolViewModel
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public bool Seleccionado { get; set; }
    }
}