﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Models.Votacion
{
    public class VotacionPlanilla
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Identidad { get; set; }
        public string Jornada { get; set; }
        public string Grado { get; set; }
        public string Seccion { get; set; }
        public int ModalidadId { get; set; }
        public string Modalidad
        {
            get { return ObtenerListadoModalidad(ModalidadId); }
            set { }
        }

        public string ObtenerListadoModalidad(int idModalidad)
        {
            return (idModalidad == 1) ? "Primaria" : "Secundaria";
        }

        public List<PlanillaEnc> lstPlanilla { get; set; }


    }


    public class PlanillaEnc
    {
        public int Id { get; set; }
        public string PlanillaNombre { get; set; }
        public string Logo { get; set; }
        public List<PlanillaDet> lstDetalle { get; set; }

    }

    public class PlanillaDet
    {
        public string Alumno { get; set; }
        public string Cargo { get;set; }
        public int CargoId { get; set; }
        public string base64Img { get; set; }

    }


}