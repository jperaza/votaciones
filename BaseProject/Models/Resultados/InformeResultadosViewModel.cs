﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Models.Resultados
{
    public class InformeResultadosViewModel
    {
        public string Estudiante { get; set; }
        public string Grado { get; set; }
        public string Jornada { get; set; }
        public string Seccion { get; set; }
        public string Planilla { get; set; }

    }
}