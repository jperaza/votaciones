﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Models.Resultados
{

    public class PlanillaEncViewModel
    {
        public int idModalidad { get; set; }
        public string Modalidad { 
            get { return ObtenerListadoModalidad(idModalidad); }
            set { }
        }
        public int CantidadVotosTotales { get; set; }

        public string ObtenerListadoModalidad(int idModalidad)
        {
            return (idModalidad == 1) ? "Primaria" : "Secundaria";
        }

        public List<ResultadosPlanilla> lstPlanilla { get; set; }

    }

    public class ResultadosPlanilla
    {
        public string Planilla { get; set; }
        public int Total { get; set; }
        public int idModalidad { get; set; }
    }
}