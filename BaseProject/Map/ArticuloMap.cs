﻿using AutoMapper;
using BaseProject.Data;
using BaseProject.Models.Articulo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Map
{
    public class ArticuloMap : Profile
    {
        public ArticuloMap()
        {
            CreateMap<Articulo, CrearArticuloViewModel>()
            .ForMember(d => d.Imagen, o => o.Ignore())
            ;

            CreateMap<CrearArticuloViewModel, Articulo>()
            .ForMember(d => d.Activo, o => o.MapFrom(z => true))
            .ForMember(d => d.Publicado, o => o.MapFrom(z => z.Publicado))
            .ForMember(d => d.FechaCreacion, o => o.MapFrom(z => DateTime.Now))
            .ForMember(d => d.FechaPublicacion, o => o.MapFrom(z => DateTime.Now))
            .ForMember(d => d.Imagen, o => o.Ignore())
            ;

            CreateMap<Articulo, ListarArticuloViewModel>()
                 .ForMember(d => d.NombreSeccion, o => o.MapFrom(z => z.Seccion.Titulo))
                 .ForMember(d => d.Imagen, o => o.Ignore())
                 .ForMember(d => d.ImageBase64, o => o.MapFrom(z => string.Format("data:image/gif;base64,{0}", Convert.ToBase64String(z.Imagen ?? new byte[byte.MinValue]))));

           

        }
    }
}