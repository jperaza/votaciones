﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BaseProject.Data;
using BaseProject.Models.Seccion;

namespace BaseProject.Map
{
    public class SeccionMap : Profile
    {
        public SeccionMap()
        {
            CreateMap<Seccion, SeccionViewModel>()
            .ForMember(d => d.Activo, o => o.MapFrom(z => z.Activo));

            CreateMap<SeccionViewModel, Seccion>()
            .ForMember(d => d.Activo, o => o.MapFrom(z => true))
            .ForMember(d => d.Publicado, o => o.MapFrom(z => true))
            ;

            CreateMap<Seccion, ListarSeccionViewModel>();


        }
    }
}