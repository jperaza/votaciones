﻿using AutoMapper;
using BaseProject.DB;
using BaseProject.Models.Votacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Map
{
    public class PlanillaDetMap : Profile
    {
        public PlanillaDetMap()
        {
            CreateMap<Planilla_Detalle, PlanillaDet>()
                .ForMember(dest => dest.Alumno,
                            opt => opt.MapFrom(src => src.Alumnos.Nombre))
                .ForMember(dest => dest.Cargo,
                            opt => opt.MapFrom(src => src.Cargos.Descripcion))
                .ForMember(dest => dest.CargoId,
                            opt => opt.MapFrom(src => src.CargoId))
                .ForMember(dest => dest.base64Img,
                            opt => opt.MapFrom(src => string.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(src.AdjuntoStram))));
                
        }
    }
}