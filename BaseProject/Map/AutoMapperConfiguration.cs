﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseProject.Map
{
    public static class AutoMapperConfiguration
    {
        public static void Configure()
        {

            AutoMapper.Mapper.Initialize(x =>
            {          
                x.AddProfile<SeccionMap>();
                x.AddProfile<ArticuloMap>();
 

            });

        }
    }
}